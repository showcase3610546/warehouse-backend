import { createSlice, PayloadAction } from "@reduxjs/toolkit"



type MessageState = {
    isMessage: boolean,
    type: MessageType,
    message: string,
    color: string,
}

enum MessageType {
    SUCCESS = "success",
    ERROR = "error"
}

type InitialState = {
    value: MessageState;
}

const initialState = {
    value: {
        isMessage: false,
        type: MessageType.SUCCESS,
        message: "",
        color: "#34D399"
    } as MessageState,
} as InitialState;


export const alert = createSlice({
    name: "auth",
    initialState,
    reducers: {
        clearMessage: () => {
            return initialState
        },
        successMessage: (state, action: PayloadAction<string>) => {
            return {
                value: {
                    isMessage: true,
                    type: MessageType.SUCCESS,
                    message: action.payload,
                    color: "#34D399"
                }
            };
        },
        errorMessage: (state, action: PayloadAction<string>) => {
            return {
                value: {
                    isMessage: true,
                    type: MessageType.ERROR,
                    message: action.payload,
                    color: "#B45454"
                }
            };
        }
    }
})

export const { clearMessage, successMessage, errorMessage } = alert.actions;

export default alert.reducer;