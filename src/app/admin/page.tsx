"use client";

import React, { useState, FormEvent } from "react";
import DefaultLayout from "@/components/Layouts/DefaultLayout";

const User: React.FC = () => {
    return (
        <DefaultLayout>
            <div>
                <h1 className="font-semibold text-lg">Welcome to warehouse admin panel</h1>
            </div>
        </DefaultLayout>

    )
}

export default User;