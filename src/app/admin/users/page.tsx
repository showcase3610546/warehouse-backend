"use client";

import React, { useState, FormEvent, useEffect } from "react";
import DefaultLayout from "@/components/Layouts/DefaultLayout";
import Image from "next/image";
import { BRAND } from "@/types/brand";
import Link from "next/link";
import Breadcrumb from "@/components/Breadcrumbs/Breadcrumb";
import { useSession } from "next-auth/react"

const User: React.FC = () => {
    interface User {
        id: number,
        name: string,
        email: string,
        createdAt: string
    }
    const { data: session } = useSession()
    const [users, setUsers] = useState<User[]>([]);

    useEffect(() => {
        fetch(process.env.NEXT_PUBLIC_API_URL + '/api/users', {
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${session?.user?.token}`
            }
        })
            .then((res) => res.json())
            .then((data) => {
                setUsers(data.users.data)
            });
    }, [])

    return (
        <DefaultLayout>
            <Breadcrumb pageName="User" />
            <div className="rounded-sm border border-stroke bg-white px-5 pb-2.5 pt-6 shadow-default dark:border-strokedark dark:bg-boxdark sm:px-7.5 xl:pb-1">
                <div className="flex justify-between mb-4">
                    <h4 className="mb-6 text-xl font-semibold text-black dark:text-white">
                        User
                    </h4>
                    <Link
                        href="#"
                        className="inline-flex items-center justify-center bg-primary px-10 py-4 text-center font-medium text-white hover:bg-opacity-90 lg:px-8 xl:px-10"
                    >
                        Create
                    </Link>
                </div>


                <div className="flex flex-col">
                    <div className="grid grid-cols-3 rounded-sm bg-gray-2 dark:bg-meta-4 sm:grid-cols-3">
                        <div className="p-2.5 xl:p-5">
                            <h5 className="text-sm font-medium uppercase xsm:text-base">
                                Name
                            </h5>
                        </div>
                        <div className="p-2.5 xl:p-5">
                            <h5 className="text-sm font-medium uppercase xsm:text-base">
                                Email
                            </h5>
                        </div>
                        <div className="p-2.5 xl:p-5">
                            <h5 className="text-sm font-medium uppercase xsm:text-base">
                                Created At
                            </h5>
                        </div>
                    </div>

                    {users.map((user, key) => (
                        <div
                            className={`grid grid-cols-3 sm:grid-cols-3 ${key === users.length - 1
                                ? ""
                                : "border-b border-stroke dark:border-strokedark"
                                }`}
                            key={key}
                        >
                            <div className="flex items-center justify-start p-2.5 xl:p-5">
                                <p className="text-black dark:text-white">{user.name}</p>
                            </div>
                            <div className="flex items-center justify-start p-2.5 xl:p-5">
                                <p className="text-black dark:text-white">{user.email}</p>
                            </div>
                            <div className="flex items-center justify-start p-2.5 xl:p-5">
                                <p className="text-black dark:text-white">{user.createdAt}</p>
                            </div>

                        </div>
                    ))}
                </div>
            </div>
        </DefaultLayout>

    )
}

export default User;