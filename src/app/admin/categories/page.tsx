"use client";

import React, { useState, FormEvent, useEffect } from "react";
import DefaultLayout from "@/components/Layouts/DefaultLayout";
import Image from "next/image";
import { BRAND } from "@/types/brand";
import Link from "next/link";
import Breadcrumb from "@/components/Breadcrumbs/Breadcrumb";
import { useSession } from "next-auth/react"
import { Category } from "@/types/category"

const User: React.FC = () => {
    interface Category {
        id: number,
        name: string,
        createdAt: string
    }
    const { data: session } = useSession()
    const [categories, setCategories] = useState<Category[]>([]);
    const [pagination, setPagination] = useState<any>();
    const [page, setPage] = useState(1);

    useEffect(() => {
        setCategory()
    }, [page])

    const setCategory = async () => {
        return fetch(process.env.NEXT_PUBLIC_API_URL + `/api/categories?page=${page}&limit=5`, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${session?.user?.token}`
            }
        })
            .then((res) => res.json())
            .then((data) => {
                if (page > 1 && data.categories.data.length < 1) {
                    setPage(page - 1);
                } else {
                    setCategories(data.categories.data)
                    setPagination(data.categories.meta);
                }

            });
    }

    const handleDelete = async (category: Category) => {
        fetch(process.env.NEXT_PUBLIC_API_URL + `/api/categories/${category.id}`, {
            method: 'DELETE',
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${session?.user?.token}`
            }
        })
            .then((res) => res.json())
            .then((data) => {
                setCategory()
            });
    }

    return (
        <DefaultLayout>
            <Breadcrumb pageName="Category" />
            <div className="rounded-sm border border-stroke bg-white px-5 pb-4 pt-6 shadow-default dark:border-strokedark dark:bg-boxdark sm:px-7.5 xl:pb-6">
                <div className="flex justify-between mb-4">
                    <h4 className="mb-6 text-xl font-semibold text-black dark:text-white">
                        Category
                    </h4>
                    <Link
                        href="/admin/categories/create"
                        className="inline-flex items-center justify-center bg-primary px-10 py-4 text-center font-medium text-white hover:bg-opacity-90 lg:px-8 xl:px-10"
                    >
                        Create
                    </Link>
                </div>


                <div className="flex flex-col">
                    <div className="grid grid-cols-3 rounded-sm bg-gray-2 dark:bg-meta-4 sm:grid-cols-3">
                        <div className="p-2.5 xl:p-5">
                            <h5 className="text-sm font-medium uppercase xsm:text-base">
                                Name
                            </h5>
                        </div>
                        <div className="p-2.5 xl:p-5">
                            <h5 className="text-sm font-medium uppercase xsm:text-base">
                                Created At
                            </h5>
                        </div>
                        <div className="p-2.5 xl:p-5">
                            <h5 className="text-sm font-medium uppercase xsm:text-base">
                                Action
                            </h5>
                        </div>
                    </div>

                    {categories.map((category, key) => {
                        const [date, timeZone] = category.createdAt.split('T');
                        const [timePoint] = timeZone.split('+');
                        const time = timePoint.split('.')[0];
                        return (
                            <div
                                className={`grid grid-cols-3 sm:grid-cols-3 ${key === categories.length - 1
                                    ? ""
                                    : "border-b border-stroke dark:border-strokedark"
                                    }`}
                                key={key}
                            >
                                <div className="flex items-center justify-start p-2.5 xl:p-5">
                                    <p className="text-black dark:text-white">{category.name}</p>
                                </div>
                                <div className="flex items-center justify-start p-2.5 xl:p-5">
                                    <p className="text-black dark:text-white">{date} {time}</p>
                                </div>
                                <div className="flex items-center justify-start space-x-10 p-2.5 xl:p-5">
                                    <Link href={`/admin/categories/${category.id}/edit`}>
                                        <p className="text-primary">Edit</p>
                                    </Link>
                                    <div className="cursor-pointer" onClick={() => handleDelete(category)}>
                                        <p className="text-danger">Delete</p>
                                    </div>
                                </div>


                            </div>
                        )

                    })}
                </div>

                <div className="flex justify-end">
                    <nav aria-label="Page navigation">
                        <ul className="inline-flex items-center -space-x-px">
                            {
                                page > 1 &&
                                <li>
                                    <a onClick={() => setPage(page - 1)} className="px-3 py-2 ml-0 cursor-pointer leading-tight text-gray-500 bg-white dark:bg-slate-700 border border-slate-500 rounded-l-lg hover:bg-blue-500 hover:text-white dark:bg-gray-700 dark:text-gray-400 dark:border-gray-600 dark:hover:bg-blue-500 dark:hover:text-white">Previous</a>
                                </li>
                            }

                            {
                                page < pagination?.lastPage &&
                                <li>
                                    <a onClick={() => setPage(page + 1)} className="px-3 py-2 cursor-pointer leading-tight text-gray-500 bg-white dark:bg-slate-700 border border-slate-500 rounded-r-lg hover:bg-blue-500 hover:text-white dark:bg-gray-700 dark:text-gray-400 dark:border-gray-600 dark:hover:bg-blue-500 dark:hover:text-white">Next</a>
                                </li>
                            }
                        </ul>
                    </nav>
                </div>
            </div>
        </DefaultLayout>

    )
}

export default User;