"use client";

import React, { useState, FormEvent, useEffect } from "react";
import DefaultLayout from "@/components/Layouts/DefaultLayout";
import Image from "next/image";
import { BRAND } from "@/types/brand";
import Link from "next/link";
import Breadcrumb from "@/components/Breadcrumbs/Breadcrumb";
import { useSession } from "next-auth/react"
import { useRouter, useParams } from 'next/navigation'

interface FormData {
    name: string;
}

const Category: React.FC = () => {

    const { data: session } = useSession()
    const router = useRouter();
    const params = useParams<{ id: string }>()

    useEffect(() => {
        fetch(process.env.NEXT_PUBLIC_API_URL + `/api/categories/${params.id}`, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${session?.user?.token}`
            }
        }).then((res) => res.json())
            .then((data) => {
                setFormData({ ...formData, name: data.category.name });
            });
    }, [])


    const [formData, setFormData] = useState<FormData>({
        name: ''
    });
    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        setFormData({ ...formData, [name]: value });
    };

    const handleSubmit = () => {
        fetch(process.env.NEXT_PUBLIC_API_URL + `/api/categories/${params.id}`, {
            method: 'PUT',
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${session?.user?.token}`
            },
            body: JSON.stringify(formData)
        }).then((data) => {
            router.push('/admin/categories')
        });

    };

    return (
        <DefaultLayout>
            <Breadcrumb pageName="Category" />
            <div className="rounded-sm border border-stroke bg-white px-5 pb-2.5 pt-6 shadow-default dark:border-strokedark dark:bg-boxdark sm:px-7.5 xl:pb-1">

                <div>
                    {/* <!-- Input Fields --> */}
                    <div className="rounded-sm border border-stroke bg-white shadow-default dark:border-strokedark dark:bg-boxdark">
                        <div className="border-b border-stroke px-6.5 py-4 dark:border-strokedark flex justify-between">
                            <h3 className="font-medium text-black dark:text-white">
                                Create Category
                            </h3>
                            <button
                                onClick={() => handleSubmit()}
                                className="inline-flex items-center justify-center bg-primary px-10 py-4 text-center font-medium text-white hover:bg-opacity-90 lg:px-8 xl:px-10"
                            >
                                Save
                            </button>
                        </div>
                        <div className="flex flex-col gap-5.5 p-6.5">
                            <div>
                                <label className="mb-3 block text-sm font-medium text-black dark:text-white">
                                    Name
                                </label>
                                <input
                                    onChange={handleInputChange}
                                    name="name"
                                    value={formData.name}
                                    type="text"
                                    placeholder="Category Name"
                                    className="w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary"
                                />
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </DefaultLayout>

    )
}

export default Category;