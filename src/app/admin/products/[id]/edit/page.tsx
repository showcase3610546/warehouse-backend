"use client";

import React, { useState, FormEvent, useEffect } from "react";
import DefaultLayout from "@/components/Layouts/DefaultLayout";
import Image from "next/image";
import { BRAND } from "@/types/brand";
import Link from "next/link";
import Breadcrumb from "@/components/Breadcrumbs/Breadcrumb";
import { useSession } from "next-auth/react"
import { useRouter, useParams } from 'next/navigation'
import { clearMessage, successMessage, errorMessage } from "@/lib/features/alert-slice"
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, useAppSelector } from "@/lib/store";

interface FormData {
    name: string;
    sku: string;
    category_id: number;
    base_price: number;
    cost_price: number;
    initial_qty: number;
    image_url: ''
}

interface Category {
    id: number,
    name: string,
    createdAt: string
}

const Category: React.FC = () => {

    const { data: session } = useSession()
    const router = useRouter();
    const params = useParams<{ id: string }>()
    const dispatch = useDispatch<AppDispatch>();

    const [categories, setCategories] = useState<Category[]>([]);

    useEffect(() => {
        setCategory()

        fetch(process.env.NEXT_PUBLIC_API_URL + `/api/products/${params.id}`, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${session?.user?.token}`
            }
        }).then((res) => res.json())
            .then((data) => {
                console.log(data);
                setFormData({
                    ...formData,
                    name: data.product.name,
                    sku: data.product.sku,
                    category_id: data.product.categoryId,
                    base_price: data.product.basePrice,
                    cost_price: data.product.costPrice,
                    initial_qty: data.product.initialQty,
                    image_url: data.product.imageUrl
                });
            });
    }, [])

    const setCategory = async () => {
        return fetch(process.env.NEXT_PUBLIC_API_URL + '/api/categories', {
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${session?.user?.token}`
            }
        })
            .then((res) => res.json())
            .then((data) => {
                setCategories(data.categories.data)
            });
    }

    const [formData, setFormData] = useState<FormData>({
        name: '',
        sku: '',
        category_id: 0,
        base_price: 0,
        cost_price: 0,
        initial_qty: 0,
        image_url: ''

    });
    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        setFormData({ ...formData, [name]: value });
    };

    const handleSelectChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        const { name, value } = event.target;
        setFormData({ ...formData, [name]: value });
    };

    const handleSubmit = () => {
        fetch(process.env.NEXT_PUBLIC_API_URL + `/api/products/${params.id}`, {
            method: 'PUT',
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${session?.user?.token}`
            },
            body: JSON.stringify(formData)
        }).then((res) => {
            console.log(res.ok);
            if (!res.ok) {
                res.json().then((error) => {
                    dispatch(errorMessage(error.errors[0].message));
                    setTimeout(() => {
                        dispatch(clearMessage());
                    }, 2000);
                });

            } else {
                dispatch(successMessage("Successfully Update Product"));
                setTimeout(() => {
                    dispatch(clearMessage());
                }, 2000);
                router.push('/admin/products')

            }

        }).catch((error) => {
            dispatch(errorMessage("Error Update Product"));
            console.log("ERROR Occur");

        });

    };

    return (
        <DefaultLayout>
            <Breadcrumb pageName="Product" />
            <div className="rounded-sm border border-stroke bg-white px-5 pb-2.5 pt-6 shadow-default dark:border-strokedark dark:bg-boxdark sm:px-7.5 xl:pb-1">

                <div>
                    {/* <!-- Input Fields --> */}
                    <div className="rounded-sm border border-stroke bg-white shadow-default dark:border-strokedark dark:bg-boxdark">
                        <div className="border-b border-stroke px-6.5 py-4 dark:border-strokedark flex justify-between">
                            <h3 className="font-medium text-black dark:text-white">
                                Update Product
                            </h3>
                            <button
                                onClick={() => handleSubmit()}
                                className="inline-flex items-center justify-center bg-primary px-10 py-4 text-center font-medium text-white hover:bg-opacity-90 lg:px-8 xl:px-10"
                            >
                                Save
                            </button>
                        </div>
                        <div className="flex flex-col gap-5.5 p-6.5">
                            <div className="flex justify-between">
                                <div className="w-full">
                                    <label className="mb-3 block text-sm font-medium text-black dark:text-white">
                                        Name
                                    </label>
                                    <input
                                        onChange={handleInputChange}
                                        name="name"
                                        value={formData.name}
                                        type="text"
                                        placeholder="Product Name"
                                        className="w-2/3 rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary"
                                    />
                                </div>
                                <div className="w-full">
                                    <label className="mb-3 block text-sm font-medium text-black dark:text-white">
                                        SKU
                                    </label>
                                    <input
                                        onChange={handleInputChange}
                                        name="sku"
                                        value={formData.sku}
                                        type="text"
                                        placeholder="SKU"
                                        className="w-2/3  rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary"
                                    />
                                </div>
                                <div className="w-full">
                                    <label className="mb-3 block text-sm font-medium text-black dark:text-white">
                                        Select Category
                                    </label>
                                    <select
                                        name="category_id"
                                        value={formData.category_id}
                                        onChange={handleSelectChange}
                                        className={`relative z-20 w-full appearance-none rounded border border-stroke bg-transparent px-4 py-3 outline-none transition focus:border-primary active:border-primary dark:border-form-strokedark dark:bg-form-input`}
                                    >
                                        {
                                            categories.map(category => (
                                                <option key={category.id} value={category.id} className="text-body dark:text-bodydark">
                                                    {category.name}
                                                </option>
                                            ))
                                        }

                                    </select>
                                </div>
                            </div>

                            <div className="flex justify-between">
                                <div className="w-full">
                                    <label className="mb-3 block text-sm font-medium text-black dark:text-white">
                                        Base Price
                                    </label>
                                    <input
                                        onChange={handleInputChange}
                                        name="base_price"
                                        value={formData.base_price}
                                        type="text"
                                        placeholder="Base Price"
                                        className="w-2/3 rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary"
                                    />
                                </div>
                                <div className="w-full">
                                    <label className="mb-3 block text-sm font-medium text-black dark:text-white">
                                        Cost Price
                                    </label>
                                    <input
                                        onChange={handleInputChange}
                                        name="cost_price"
                                        value={formData.cost_price}
                                        type="text"
                                        placeholder="Cost Price"
                                        className="w-2/3  rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary"
                                    />
                                </div>
                                <div className="w-full">
                                    <label className="mb-3 block text-sm font-medium text-black dark:text-white">
                                        Initial Quantity
                                    </label>
                                    <input
                                        onChange={handleInputChange}
                                        name="initial_qty"
                                        value={formData.initial_qty}
                                        type="number"
                                        placeholder="Initial Quantity"
                                        className="w-2/3  rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary"
                                    />
                                </div>
                            </div>
                            <div>
                                <label className="mb-3 block text-sm font-medium text-black dark:text-white">
                                    Image Url
                                </label>
                                <input
                                    onChange={handleInputChange}
                                    name="image_url"
                                    value={formData.image_url}
                                    type="string"
                                    placeholder="Image Url"
                                    className="w-2/3 rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary"
                                />
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </DefaultLayout>

    )
}

export default Category;