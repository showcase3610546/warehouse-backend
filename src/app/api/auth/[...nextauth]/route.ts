import CredentialsProvider from "next-auth/providers/credentials"
import NextAuth from "next-auth"
import type { AuthOptions } from "next-auth"
import authOptions from "./options"


const handler = NextAuth(authOptions)

export { handler as GET, handler as POST }
