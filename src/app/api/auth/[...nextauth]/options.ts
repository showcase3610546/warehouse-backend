
import CredentialsProvider from "next-auth/providers/credentials"
import NextAuth from "next-auth"
import type { AuthOptions } from "next-auth"

const authOptions: AuthOptions = {
    providers: [
        CredentialsProvider({
            // The name to display on the sign in form (e.g. 'Sign in with...')
            name: 'Credentials',
            // The credentials is used to generate a suitable form on the sign in page.
            // You can specify whatever fields you are expecting to be submitted.
            // e.g. domain, username, password, 2FA token, etc.
            // You can pass any HTML attribute to the <input> tag through the object.
            credentials: {},
            async authorize(credentials, req) {
                // You need to provide your own logic here that takes the credentials
                // submitted and returns either a object representing a user or value
                // that is false/null if the credentials are invalid.
                // e.g. return { id: 1, name: 'J Smith', email: 'jsmith@example.com' }
                // You can also use the `req` object to obtain additional parameters
                // (i.e., the request IP address)
                const res = await fetch(process.env.NEXT_PUBLIC_API_URL + "/api/login", {
                    method: 'POST',
                    body: JSON.stringify(credentials),
                    headers: { "Content-Type": "application/json" }
                })
                const user = await res.json()

                console.log(user);

                // If no error and we have user data, return it
                if (res.ok && user) {
                    return user;
                }
                // Return null if user data could not be retrieved
                throw new Error(user.errors[0].message);
            }
        })
    ],
    pages: {
        signIn: "/auth/signin",
        signOut: "/auth/signin"
    },

    callbacks: {
        async jwt({ token, user }) {
            // Persist the OAuth access_token to the token right after signin
            if (user) {
                token.user = user.user
                token.token = user.token
            }
            return token;
        },
        async session({ session, token }) {
            // Send properties to the client, like an access_token from a provider.
            session.user.user = token.user;
            session.user.token = token.token;
            return session
        },

        // signIn: async ({ user }) => {
        //     if (user) {
        //         return Promise.resolve(true);
        //     }
        // },

        async redirect({ url, baseUrl }) {
            // if (url.startsWith(`${baseUrl}/auth/signin`)) return `${baseUrl}/admin`
            // Allows relative callback URLs
            if (url.startsWith("/")) return `${baseUrl}${url}`
            // Allows callback URLs on the same origin
            else if (new URL(url).origin === baseUrl) return url
            return baseUrl
        }
    }
}

export default authOptions;